module input_subprogs
    implicit none
    contains
    subroutine input(numpoints, a, numcells, numcellpoints, p)
        integer numpoints, numcells, numcellpoints
        integer, allocatable :: p(:,:)
        integer :: i, dim = 2
        real(8), allocatable :: a(:,:)

        open(12, file = "node.dat", status="old") !修正済（節点が4つ以上の時は？）←
        read(12,*) numpoints
        allocate (a(numpoints, dim)) !割付を忘れない！
        do i = 1, numpoints
            read(12,*) a(i, 1:dim)
        enddo
        close(12)

        open(13, file = "elem.dat" , status="old") !修正済（要素が複数にわたるときは？）
        read(13,*) numcells, numcellpoints
        allocate (p(numcells, numcellpoints))
        do i = 1, numcells
            read(13,*) p(i, 1:numcellpoints)
        enddo
        close(13)

    end subroutine input
end module input_subprogs

module output_subprogs
    implicit none
    contains
    subroutine output(numpoints, a, numcells, numcellpoints, p)
        integer numpoints, numcells, numcellpoints, offsets
        integer :: i, j, dim = 2 ! node.datで与えられる節点の座標は２次元である
        real(8), allocatable :: a(:,:)
        integer, allocatable :: p(:,:)
        character(100) filename, n

        write(n, '(i0)') numcells
        filename = trim(n) // "elem.vtu"

        open(10, file = trim(filename) , status = "replace")

        write(10,'(a)') "<?xml version=""1.0""?>"
        write(10,'(a)') "<VTKFile type=""UnstructuredGrid"" version=""1.0"">"
        write(10,'(a)') "<UnstructuredGrid>"

        write(10,'(a,i0,a,i0,a)') "<Piece NumberOfPoints=""", numpoints, """ NumberOfCells=""", numcells, """>"

        write(10,'(a)') "<Points>" ! 節点 一筆書きの順番
        write(10,'(a,i0,a)') "<DataArray type=""Float32"" NumberOfComponents=""", 3, """ format=""ascii"">" !2次元なので。。。→2だとうまく可視化できなかった
        do i = 1, numpoints
            write(10,*) a(i, 1:dim), 0.0d0 !NOCを3にして、ここの後に0.0d0を続けると3次元
                enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a)') "</Points>"

        write(10,'(a)') "<Cells>" ! 要素
        write(10,'(a,i0,a)') "<DataArray type=""Int32"" Name=""connectivity"" format=""ascii"">" ! 要素ごとの節点番号
        do i = 1, numcells ! 修正済、整数が左詰め・半角英数字を挟むようにした
            do j = 1, numcellpoints
            if (j /= numcellpoints) then
                write(10,'(i0, a)', advance = 'no') p(i, j) - 1, " " !この記述でよい、可視化には0スタートである必要があるが、Fortranの特性上入力ファイルは１スタート
            else
                write(10,'(i0)') p(i, j) - 1
            endif
            enddo
        enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a,i0,a)') "<DataArray type=""Int32"" Name=""offsets"" format=""ascii"">"
        do i = 1, numcells !各要素の終わりの要素番号を指定
            offsets = i * numcellpoints !要素番号=各要素の節点数じゃないかも
            if (i /= numcells) then
            write(10,'(i0,a)', advance = 'no') offsets, " " !advanceなくてもいいかも
            else
            write(10,'(i0)') offsets
            endif
        enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a,i0,a)') "<DataArray type=""UInt8"" Name=""types"" format=""ascii"">" ! 要素の種別番号（四角形は4）
        do i = 1, numcells
            if (i /= numcells) then
            write(10,'(i0,a)', advance = 'no') 9, " " !advanceなくてもいいかも
            else
            write(10,'(i0)') 9
            endif
            enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a)') "</Cells>"

        write(10,'(a)') "</Piece>"
        write(10,'(a)') "</UnstructuredGrid>"
        write(10,'(a)') "</VTKFile>"
        close(10)
    end subroutine output
end module output_subprogs

program main
    use input_subprogs
    use output_subprogs
    implicit none
    integer numpoints, numcells, numcellpoints
    integer, allocatable :: p(:,:)
    real(8), allocatable :: a(:,:)

    call input(numpoints, a, numcells, numcellpoints, p)
    call output(numpoints, a, numcells, numcellpoints, p)

end program main

