module invert_subprogs !逆行列計算
  implicit none
  contains
  subroutine invert(a)
    real(8) a(:, :)
    real(8) c, dum
    integer j, k, n, i

    n = int(sqrt(real(size(a)))) !aの配列の個数→平方根をとる
    do k = 1, n
      c = a(k, k)
      a(k, k) = 1.0d0
      do j = 1, n
        a(k, j) = a(k, j) / c
      enddo
      do i = 1, n
        if(i /= k) then
          dum = a(i, k)
          a(i, k) = 0.0d0
          do j = 1, n
            a(i, j) = a(i, j) - dum * a(k, j)
          enddo
        endif
      enddo
    enddo

  end subroutine invert
end module invert_subprogs

! 固有値を求めるモジュールが欲しい

module input_subprogs
  implicit none
  contains
  subroutine input(young, poisson, numpoints, a, numcells, numcellpoints, p, u, f)
    integer numpoints, numcells, numcellpoints
    integer, allocatable :: p(:,:)
    integer :: i, dim = 2
    integer numbc, numfree, free, id
    integer numload
    real(8) young, poisson
    real(8), allocatable :: a(:,:)
    real(8), allocatable :: u(:)
    real(8), allocatable :: f(:)
    real(8) bc, load

    open(11, file = "input.dat" , status="old")
      read(11,*) young
      read(11,*) poisson
    close(11)

    open(12, file = "node.dat", status="old") !修正済（節点が4つ以上の時は？）←
      read(12,*) numpoints
      allocate (a(numpoints, dim)) !割付を忘れない！
      do i = 1, numpoints
        read(12,*) a(i, 1:dim)
      enddo
    close(12)

    open(13, file = "elem.dat" , status="old") !修正済（要素が複数にわたるときは？）
      read(13,*) numcells, numcellpoints
      allocate (p(numcells, numcellpoints))
      do i = 1, numcells
        read(13,*) p(i, 1:numcellpoints)
      enddo
    close(13)

    open(14, file = "bc.dat" , status="old") ! ノイマン境界条件付与 正直まだわからん
      read(14,*) numbc, numfree
      allocate (u(dim*numpoints))
      u(:) = 1.0d0 ! いいの？
      do i = 1, numbc
        read(14,*) id, free, bc
        if (free == 1) then
          u(2 * id - 1) = dble(bc)
        elseif (free == 2) then
          u(2 * id) = dble(bc)
        endif
      enddo
    close(14)

    open(15, file = "load.dat" , status="old") ! ディリクレ境界条件付与
      read(15,*) numload, numfree
      allocate (f(dim*numpoints))
      f(:) = 0.0d0
      do i = 1, numload
        read(15,*) id, free, load
        if (free == 1) then
          f(2 * id - 1) = dble(load)
        elseif (free == 2) then
          f(2 * id) = dble(load)
        endif
      enddo
    close(15)

  end subroutine input
end module input_subprogs

module output_subprogs
    implicit none
    contains
    subroutine output(numpoints, a, numcells, numcellpoints, p, u, eps, sig)
        integer numpoints, numcells, numcellpoints, offsets
        integer, allocatable :: p(:,:)
        integer :: i, j, dim = 2 ! node.datで与えられる節点の座標は２次元である
        real(8), allocatable :: a(:,:)
        real(8), allocatable :: u(:,:)
        real(8), allocatable :: eps(:,:)
        real(8), allocatable :: sig(:,:)
        character(100) filename, n

        write(n, '(i0)') numcells
        filename = trim(n) // "elem_output.vtu"

        open(10, file = trim(filename) , status = "replace")

        write(10,'(a)') "<?xml version=""1.0""?>"
        write(10,'(a)') "<VTKFile type=""UnstructuredGrid"" version=""1.0"">"
        write(10,'(a)') "<UnstructuredGrid>"

        write(10,'(a,i0,a,i0,a)') "<Piece NumberOfPoints=""", numpoints, """ NumberOfCells=""", numcells, """>"

        write(10,'(a)') "<Points>" ! 節点 一筆書きの順番
        write(10,'(a,i0,a)') "<DataArray type=""Float32"" NumberOfComponents=""", 3, """ format=""ascii"">" !2次元なので。。。→2だとうまく可視化できなかった
        do i = 1, numpoints
          write(10,*) a(i, 1:dim), 0.0d0 !NOCを3にして、ここの後に0.0d0を続けると3次元
        enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a)') "</Points>"

        write(10,'(a)') "<Cells>" ! 要素
        write(10,'(a,i0,a)') "<DataArray type=""Int32"" Name=""connectivity"" format=""ascii"">" ! 要素ごとの節点番号
        do i = 1, numcells ! 修正済、整数が左詰め・半角英数字を挟むようにした
          do j = 1, numcellpoints
            if (j /= numcellpoints) then
              write(10,'(i0, a)', advance = 'no') p(i, j) - 1, " " !この記述でよい、可視化には0スタートである必要があるが、Fortranの特性上入力ファイルは１スタート
            else
              write(10,'(i0)') p(i, j) - 1
            endif
          enddo
        enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a,i0,a)') "<DataArray type=""Int32"" Name=""offsets"" format=""ascii"">"
        do i = 1, numcells !各要素の終わりの要素番号を指定
          offsets = i * numcellpoints !要素番号=各要素の節点数じゃないかも
          if (i /= numcells) then
            write(10,'(i0,a)', advance = 'no') offsets, " " !advanceなくてもいいかも
          else
            write(10,'(i0)') offsets
          endif
        enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a,i0,a)') "<DataArray type=""UInt8"" Name=""types"" format=""ascii"">" ! 要素の種別番号（四角形は4）
        do i = 1, numcells
          if (i /= numcells) then
            write(10,'(i0,a)', advance = 'no') 9, " " !advanceなくてもいいかも
          else
            write(10,'(i0)') 9
          endif
          enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a)') "</Cells>"

        write(10,'(a)') "<PointData>" ! 変位
        write(10,'(a,i0,a)') "<DataArray type=""Float32"" Name=""Displacement"" NumberOfComponents=""", 3, """ format=""ascii"">"
        do i = 1, numpoints
          write(10,*) u(i, 1:dim), 0.0d0
        enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a)') "</PointData>"

        write(10,'(a)') "<CellData>"
        write(10,'(a,i0,a)') "<DataArray type=""Float32"" Name=""Strain"" NumberOfComponents=""", 9, """ format=""ascii"">" ! ひずみ
        do i = 1, numcells
          write(10,*) eps(i, 1), eps(i, 3), 0.0d0
          write(10,*) eps(i, 3), eps(i, 2), 0.0d0
          write(10,*) 0.0d0, 0.0d0, 0.0d0
        enddo
        write(10,'(a)') "</DataArray>"

        write(10,'(a,i0,a)') "<DataArray type=""Float32"" Name=""Stress"" NumberOfComponents=""", 9, """ format=""ascii"">" ! 応力
        do i = 1, numcells
          write(10,*) sig(i, 1), sig(i, 3), 0.0d0
          write(10,*) sig(i, 3), sig(i, 2), 0.0d0
          write(10,*) 0.0d0, 0.0d0, 0.0d0
        enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a)') "</CellData>"

        write(10,'(a)') "</Piece>"
        write(10,'(a)') "</UnstructuredGrid>"
        write(10,'(a)') "</VTKFile>"
      close(10)
    end subroutine output
end module output_subprogs

module culculate_k_subprogs !要素→全体剛性行列作成
  !$ use omp_lib
  use invert_subprogs
    implicit none
    contains
    subroutine culculate_k(young, poisson, numcells, numpoints, numcellpoints, a, p, k, d)
        real(8) young, poisson
        real(8), allocatable :: a(:,:)
        integer, allocatable :: p(:,:)
        real(8), allocatable :: aa(:,:)
        real(8), allocatable :: ke(:,:)
        real(8), allocatable :: k(:,:)
        real(8), allocatable :: b(:,:)
        real(8) d(3, 3), dNxe(2, 4), j(2, 2), bt(8, 3)
        real(8) dN1dx, dN2dx, dN3dx, dN4dx, dN1dy, dN2dy, dN3dy, dN4dy
        real(8) xi(2), eta(2), detj
        integer i, m, n
        integer numcells, numpoints, numcellpoints
        integer :: dim = 2

        allocate (aa(numcellpoints, dim))
        allocate (ke(dim * numcellpoints, dim * numcellpoints)) ! 要素剛性行列 1節点自由度 * 1要素節点数 (8, 8)
        allocate (k(dim * numpoints, dim * numpoints)) ! 全体剛性行列 1節点自由度 * 総節点数
        allocate (b(3, 8)) ! Bマトリクス、ひずみ計算のために要素ごとに保存

        ! Dマトリックス計算 今回は平面応力状態
        d(1, 1) = 1.0d0 / (1.0d0 - poisson)
        d(1, 2) = poisson / (1.0d0 - poisson)
        d(1, 3) = 0.0d0
        d(2, 1) = d(1, 2)
        d(2, 2) = d(1, 1)
        d(2, 3) = 0.0d0
        d(3, 1) = 0.0d0
        d(3, 2) = 0.0d0
        d(3, 3) = 0.5d0
        do m = 1, 3
          do n = 1, 3
            d(m, n) = (young / (1 + poisson)) * d(m, n) !d(3,3)
          enddo
        enddo

      xi(1)  = -0.577350296 ! 積分点の定義
      xi(2)  =  0.577350296
      eta(1) = -0.577350296
      eta(2) =  0.577350296

      k(:,:) = 0.0d0 ! 全体剛性行列ゼロクリア

      do i = 1, numcells ! 要素抜出し,i番目の要素について

        do m = 1, numcellpoints
          do n = 1, dim
          aa(m, n) = a(p(i, m), n) !グローバル節点→ローカル節点
          enddo
        enddo

        ! ある要素について、各積分点における Jマトリックス→Bマトリックス→要素剛性
        ke(:,:) = 0.0d0 ! 要素剛性行列ゼロクリア
        do m = 1, 2 ! 各積分点についてループ
          do n = 1, 2

            dNxe(:, :) = 0.0d0
            dNxe(1, 1) = - (1.0d0 - eta(m)) / 4.0d0
            dNxe(1, 2) = (1.0d0 - eta(m)) / 4.0d0
            dNxe(1, 3) = (1.0d0 + eta(m)) / 4.0d0
            dNxe(1, 4) = - (1.0d0 + eta(m)) / 4.0d0
            dNxe(2, 1) = - (1.0d0 - xi(n)) / 4.0d0
            dNxe(2, 2) = - (1.0d0 + xi(n)) / 4.0d0
            dNxe(2, 3) = (1.0d0 + xi(n)) / 4.0d0
            dNxe(2, 4) = (1.0d0 - xi(n)) / 4.0d0

            j(:,:) = matmul(dNxe, aa) !Jマトリックス (2, 2)

            detj = j(1, 1) * j(2, 2) - j(2, 1) * j(1, 2) ! 固有値計算

            call invert(j) ! 逆行列計算

            dN1dx = j(1, 1) * dNxe(1, 1) + j(1, 2) * dNxe(2, 1)
            dN1dy = j(2, 1) * dNxe(1, 1) + j(2, 2) * dNxe(2, 1)
            dN2dx = j(1, 1) * dNxe(1, 2) + j(1, 2) * dNxe(2, 2)
            dN2dy = j(2, 1) * dNxe(1, 2) + j(2, 2) * dNxe(2, 2)
            dN3dx = j(1, 1) * dNxe(1, 3) + j(1, 2) * dNxe(2, 3)
            dN3dy = j(2, 1) * dNxe(1, 3) + j(2, 2) * dNxe(2, 3)
            dN4dx = j(1, 1) * dNxe(1, 4) + j(1, 2) * dNxe(2, 4)
            dN4dy = j(2, 1) * dNxe(1, 4) + j(2, 2) * dNxe(2, 4)

            ! Bマトリックス計算
            b(:, :) = 0.0d0
            b(1, 1) = dN1dx
            b(1, 3) = dN2dx
            b(1, 5) = dN3dx
            b(1, 7) = dN4dx
            b(2, 2) = dN1dy
            b(2, 4) = dN2dy
            b(2, 6) = dN3dy
            b(2, 8) = dN4dy
            b(3, 1) = dN1dy
            b(3, 2) = dN1dx
            b(3, 3) = dN2dy
            b(3, 4) = dN2dx
            b(3, 5) = dN3dy
            b(3, 6) = dN3dx
            b(3, 7) = dN4dy
            b(3, 8) = dN4dx

            bt = transpose(b(:, :)) ! BT(8, 3) 転置
            bt = matmul(bt, d) ! BT * D

            ke = ke + matmul(bt, b(:,:)) * detj ! Ke(8, 8) = (BT * D) * B * detJ 要素剛性行列完成

          enddo
        enddo ! 全積分点について完了

        ! 全体剛性行列作成 割とゴリ押し
        do m = 1, numcellpoints  !2行ごと、4回（8行分）
          k(2 * p(i, m) - 1, 2 * p(i, 1) - 1) = k(2 * p(i, m) - 1, 2 * p(i, 1) - 1) + ke(2 * m - 1, 1)
          k(2 * p(i, m) - 1, 2 * p(i, 1)    ) = k(2 * p(i, m) - 1, 2 * p(i, 1)    ) + ke(2 * m - 1, 2)
          k(2 * p(i, m) - 1, 2 * p(i, 2) - 1) = k(2 * p(i, m) - 1, 2 * p(i, 2) - 1) + ke(2 * m - 1, 3)
          k(2 * p(i, m) - 1, 2 * p(i, 2)    ) = k(2 * p(i, m) - 1, 2 * p(i, 2)    ) + ke(2 * m - 1, 4)
          k(2 * p(i, m) - 1, 2 * p(i, 3) - 1) = k(2 * p(i, m) - 1, 2 * p(i, 3) - 1) + ke(2 * m - 1, 5)
          k(2 * p(i, m) - 1, 2 * p(i, 3)    ) = k(2 * p(i, m) - 1, 2 * p(i, 3)    ) + ke(2 * m - 1, 6)
          k(2 * p(i, m) - 1, 2 * p(i, 4) - 1) = k(2 * p(i, m) - 1, 2 * p(i, 4) - 1) + ke(2 * m - 1, 7)
          k(2 * p(i, m) - 1, 2 * p(i, 4)    ) = k(2 * p(i, m) - 1, 2 * p(i, 4)    ) + ke(2 * m - 1, 8)

          k(2 * p(i, m)    , 2 * p(i, 1) - 1) = k(2 * p(i, m)    , 2 * p(i, 1) - 1) + ke(2 * m, 1)
          k(2 * p(i, m)    , 2 * p(i, 1)    ) = k(2 * p(i, m)    , 2 * p(i, 1)    ) + ke(2 * m, 2)
          k(2 * p(i, m)    , 2 * p(i, 2) - 1) = k(2 * p(i, m)    , 2 * p(i, 2) - 1) + ke(2 * m, 3)
          k(2 * p(i, m)    , 2 * p(i, 2)    ) = k(2 * p(i, m)    , 2 * p(i, 2)    ) + ke(2 * m, 4)
          k(2 * p(i, m)    , 2 * p(i, 3) - 1) = k(2 * p(i, m)    , 2 * p(i, 3) - 1) + ke(2 * m, 5)
          k(2 * p(i, m)    , 2 * p(i, 3)    ) = k(2 * p(i, m)    , 2 * p(i, 3)    ) + ke(2 * m, 6)
          k(2 * p(i, m)    , 2 * p(i, 4) - 1) = k(2 * p(i, m)    , 2 * p(i, 4) - 1) + ke(2 * m, 7)
          k(2 * p(i, m)    , 2 * p(i, 4)    ) = k(2 * p(i, m)    , 2 * p(i, 4)    ) + ke(2 * m, 8)
        enddo

      enddo ! 全要素について終了
    end subroutine culculate_k

end module culculate_k_subprogs

module bc_subprogs
  implicit none
  contains
  subroutine bc(k, u, numpoints)
    integer i
    integer numpoints
    real(8), allocatable :: u(:)
    real(8), allocatable :: k(:,:)

    do i = 1, 2 * numpoints
      if (u(i) == 0) then
        k(i, 1:2*numpoints) = 0.0d0
        k(1:2*numpoints, i) = 0.0d0
        k(i, i) = 1.0d0
      endif
    enddo

  end subroutine bc
end module bc_subprogs

module cg_subprogs ! CG法
  !$ use omp_lib
  implicit none
  contains
  subroutine cg_method(AMAT, x, b, numpoints)
    integer numpoints
    integer i, j, k
    integer N
    real(8), allocatable :: AMAT(:,:)
    real(8), allocatable :: x(:)
    real(8), allocatable :: b(:)
    real(8) p(2 * numpoints), Ap(2 * numpoints), r(2 * numpoints)
    real(8) rr0, rr1
    real(8) ALPHA, BETA
    real(8) pAp
    real(8) RESID
    real(8) :: eps = 1.0E-8

    N = 2 * numpoints

    !$omp parallel num_threads(8)
    !$omp do private(j)
    do i = 1, N
      r(i) = b(i)
      do j = 1, N
        r(i) = r(i) - AMAT(i, j) * x(j)
      enddo
      p(i) = r(i)
    enddo
    !$omp enddo

    !$omp single
    rr0 = 0.0d0
    !$omp endsingle

    !$omp do reduction(+:rr0)
    do i = 1, N
      rr0 = rr0 + r(i) * r(i)
    enddo
    !$omp enddo
    !$omp endparallel

    k = 1

    do
      !$omp parallel num_threads(8)
      !$omp do private(j)
      do i = 1, N
        Ap(i) = 0.0d0
        do j = 1, N
          Ap(i) = Ap(i) + AMAT(i, j) * p(j)
        enddo
      enddo
      !$omp enddo

      !$omp single ! どうか？
      pAp = 0.0d0
      !$omp endsingle

      !$omp do reduction(+:pAp)
      do i = 1, N
        pAp = pAp + p(i) * Ap(i)
      enddo
      !$omp enddo

      !$omp single
      ALPHA = rr0 / pAp
      !$omp endsingle

      !$omp do reduction(+:x, r)
      do i = 1, N
        x(i) = x(i) + ALPHA * p(i)
        r(i) = r(i) - ALPHA * Ap(i)
      enddo
      !$omp enddo

      !$omp single
      rr1 = 0.0d0
      !$omp endsingle

      !$omp do reduction(+:rr1)
      do i = 1, N
        rr1 = rr1 + r(i) * r(i)
      enddo
      !$omp enddo
      !$omp endparallel

      RESID = dsqrt(rr1 / rr0)
      if(RESID < eps) exit

      BETA = rr1 / rr0
      !$omp parallel num_threads(8)
      !$omp do
      do i = 1, N
        p(i) = r(i) + BETA * p(i)
      enddo
      !$omp enddo
      !$omp endparallel

      rr0 = rr1
      k = k + 1
    enddo

  end subroutine cg_method
end module cg_subprogs

module culculate_eps_sig ! ひずみ・応力計算、問題あり？
  use invert_subprogs
  implicit none
  contains
  subroutine eps_sig(eps, sig, d, u2, numcells, numcellpoints, a, p)
    integer i, j, k, N, M, numcells, numcellpoints
    integer :: dim = 2
    real(8), allocatable :: a(:,:)
    real(8), allocatable :: aa(:,:)
    integer, allocatable :: p(:,:)
    real(8), allocatable :: b(:,:,:)
    real(8)  d(3, 3)
    real(8), allocatable :: u2(:,:)
    real(8), allocatable :: ulocal(:,:)
    real(8), allocatable :: eps(:,:)
    real(8), allocatable :: sig(:,:)

    real(8) dNxe(2, 4), JMAT(2, 2), xi, eta, detj
    real(8) dN1dx, dN2dx, dN3dx, dN4dx, dN1dy, dN2dy, dN3dy, dN4dy

    N = 3
    M = dim * numcellpoints

    allocate(aa(numcellpoints, dim))
    allocate(ulocal(numcells, dim * numcellpoints))
    allocate(b(numcells, 3, 8))
    allocate(eps(numcells, N))
    allocate(sig(numcells, N))

    do i = 1, numcells ! 変位をローカル節点に合わせる
      do j = 1, numcellpoints
        ulocal(i, 2 * j - 1) = u2(p(i, j), 1) ! x座標
        ulocal(i, 2 * j)     = u2(p(i, j), 2) ! y座標
      enddo
    enddo

    do i = 1, numcells ! 各要素について、要素中心(xi=eta=0)のJ、Bマトリクスを求める
      do j = 1, numcellpoints
        aa(j, 1:2) = a(p(i, j), 1:2) ! グローバル節点→ローカル節点
      enddo

      xi = 0.0d0
      eta = 0.0d0
      dNxe(:, :) = 0.0d0
      dNxe(1, 1) = - (1.0d0 - eta) / 4.0d0
      dNxe(1, 2) = (1.0d0 - eta) / 4.0d0
      dNxe(1, 3) = (1.0d0 + eta) / 4.0d0
      dNxe(1, 4) = - (1.0d0 + eta) / 4.0d0
      dNxe(2, 1) = - (1.0d0 - xi) / 4.0d0
      dNxe(2, 2) = - (1.0d0 + xi) / 4.0d0
      dNxe(2, 3) = (1.0d0 + xi) / 4.0d0
      dNxe(2, 4) = (1.0d0 - xi) / 4.0d0

      JMAT(:,:) = matmul(dNxe, aa) ! Jマトリックス (2, 2)

      detj = JMAT(1, 1) * JMAT(2, 2) - JMAT(2, 1) * JMAT(1, 2) ! 固有値計算

      call invert(JMAT) ! 逆行列計算

      dN1dx = JMAT(1, 1) * dNxe(1, 1) + JMAT(1, 2) * dNxe(2, 1)
      dN1dy = JMAT(2, 1) * dNxe(1, 1) + JMAT(2, 2) * dNxe(2, 1)
      dN2dx = JMAT(1, 1) * dNxe(1, 2) + JMAT(1, 2) * dNxe(2, 2)
      dN2dy = JMAT(2, 1) * dNxe(1, 2) + JMAT(2, 2) * dNxe(2, 2)
      dN3dx = JMAT(1, 1) * dNxe(1, 3) + JMAT(1, 2) * dNxe(2, 3)
      dN3dy = JMAT(2, 1) * dNxe(1, 3) + JMAT(2, 2) * dNxe(2, 3)
      dN4dx = JMAT(1, 1) * dNxe(1, 4) + JMAT(1, 2) * dNxe(2, 4)
      dN4dy = JMAT(2, 1) * dNxe(1, 4) + JMAT(2, 2) * dNxe(2, 4)

      ! Bマトリックス計算
      b(i, :, :) = 0.0d0
      b(i, 1, 1) = dN1dx
      b(i, 1, 3) = dN2dx
      b(i, 1, 5) = dN3dx
      b(i, 1, 7) = dN4dx
      b(i, 2, 2) = dN1dy
      b(i, 2, 4) = dN2dy
      b(i, 2, 6) = dN3dy
      b(i, 2, 8) = dN4dy
      b(i, 3, 1) = dN1dy
      b(i, 3, 2) = dN1dx
      b(i, 3, 3) = dN2dy
      b(i, 3, 4) = dN2dx
      b(i, 3, 5) = dN3dy
      b(i, 3, 6) = dN3dx
      b(i, 3, 7) = dN4dy
      b(i, 3, 8) = dN4dx

    enddo

    do k = 1, numcells ! ひずみ計算
      do i = 1, N
        eps(k, i) = 0.0d0
        do j = 1, M
          eps(k, :) = matmul(b(k,:,:), ulocal(k,:))
        enddo
      enddo

      do i = 1, N ! 応力計算
        sig(k, i) = 0.0d0
        do j = 1, N
          sig(k, :) = matmul(d, eps(k,:))
        enddo
      enddo
    enddo

  end subroutine eps_sig
end module culculate_eps_sig

program main
  !$ use omp_lib
  use input_subprogs
  use output_subprogs
  use culculate_k_subprogs
  use bc_subprogs
  use cg_subprogs
  use culculate_eps_sig
  implicit none
  integer i
  integer :: dim = 2
  integer numpoints, numcells, numcellpoints
  integer, allocatable :: p(:,:)
  real(8) young, poisson
  real(8), allocatable :: a(:,:)
  real(8), allocatable :: k(:,:)
  real(8), allocatable :: u(:), u2(:,:)
  real(8), allocatable :: f(:)
  real(8)  d(3, 3)
  real(8), allocatable :: eps(:,:)
  real(8), allocatable :: sig(:,:)
  !$ real(8) st, en

  call input(young, poisson, numpoints, a, numcells, numcellpoints, p, u, f)
  ! call output(numpoints, a, numcells, numcellpoints, p) 一旦いらん

  !$ st = omp_get_wtime()

  call culculate_k(young, poisson, numcells, numpoints, numcellpoints, a, p, k, d)

  write(*,*) "K Matrix"
  ! do i = 1, 2 * numpoints
  ! write(*,*) k(i , 1 : 2 * numpoints) ! 全体剛性行列表示
  ! enddo

  call bc(k, u, numpoints) ! 境界条件付与

  write(*,*) "K Matrix'" ! 空白
  ! do i = 1, 2 * numpoints
  !  write(*,*) k(i, 1 : 2 * numpoints) ! 付与後の全体剛性行列表示
  !enddo

  u(:) = 0.0d0 ! x0設定
  call cg_method(k, u, f, numpoints) ! CG法による連立1次方程式求解

  write(*,*) "u = "
  write(*,*) u(1 : 2 * numpoints) ! 変位表示

  allocate (u2(numpoints, dim)) ! ベクトル→行列
  do i = 1, numpoints
    u2(i, 1) = u(2 * i - 1)
    u2(i, 2) = u(2 * i)
  enddo
  ! a = a + u2 ! 座標更新

  call eps_sig(eps, sig, d, u2, numcells, numcellpoints, a, p) ! ひずみ、応力計算

  write(*,*) "eps = " ! ひずみ表示
  do i = 1, numcells
    write(*,*) eps(i, 1 : 3)
  enddo

  write(*,*) "sig = " ! 応力表示
  do i = 1, numcells
    write(*,*) sig(i, 1 : 3)
  enddo

  call output(numpoints, a, numcells, numcellpoints, p, u2, eps, sig) ! 力を与えた後の形状可視化

  !$ en = omp_get_wtime()
  !$ write(*,*) "Elapsed time :", en - st

end program main
