program cg ! CG法
    implicit none
      integer :: numpoints = 3
      integer i, j, k
      integer N
      real(8) AMAT(3, 3)
      real(8) x(3)
      real(8) b(3)
      real(8) p(3), Ap(3), r(3)
      real(8) rr0, rr1
      real(8) ALPHA, BETA
      real(8) pAp
      real(8) RESID
      real(8) :: eps = 0.004d0

      N = numpoints

      AMAT(1, 1:3) = (/ 5.0d0, 0.5d0, 1.0d0 /)
      AMAT(2, 1:3) = (/ -1.0d0, -2.0d0, 0.6d0 /)
      AMAT(3, 1:3) = (/ 1.5d0, -1.0d0, 4.0d0 /)
      x(:) = 0.0d0
      b(1:3) = (/ 1.0d0, -0.5d0, 0.5d0 /)

      do i = 1, N
        r(i) = b(i)
        do j = 1, N
          r(i) = r(i) - AMAT(i, j) * x(j)
        enddo
        p(i) = r(i)
      enddo

      rr0 = 0.0d0
      do i = 1, N
        rr0 = rr0 + r(i) * r(i)
      enddo

      k = 1

      do
        do i = 1, N
          Ap(i) = 0.0d0
          do j = 1, N
            Ap(i) = Ap(i) + AMAT(i, j) * p(j)
          enddo
        enddo

        pAp = 0.0d0
        do i = 1, N
          pAp = pAp + p(i) * Ap(i)
        enddo
        ALPHA = rr0 / pAp

        do i = 1, N
          x(i) = x(i) + ALPHA * p(i)
          r(i) = r(i) - ALPHA * Ap(i)
        enddo

        write(*,*) x(1:3)

        rr1 = 0.0d0
        do i = 1, N
          rr1 = rr1 + r(i) * r(i)
        enddo

        RESID = dsqrt(rr1 / rr0)

        write(*,*) resid

        if(RESID < eps) exit

        BETA = rr1 / rr0
        do i = 1, N
          p(i) = r(i) + BETA * p(i)
        enddo

        rr0 = rr1

        k = k + 1

      enddo

end program cg