program main
    implicit none
    integer numpoints, numcells
    integer :: numcellpoints = 4
    integer n, i, j
    integer, allocatable :: p(:,:)
    real(8), allocatable :: rn(:), r(:,:), theta(:), x(:), y(:), a(:,:)
    real(8) pi, dtheta, r0, rnn, s, pow, pow0, ppow
    real(8) kouhi

    write(*,*) "1辺当たりの分割数："
    read(*,*) n

    numpoints = (2 * n ** 2) + (3 * n) + 1 + ((n + 1) * n) ! 末項は四角形部分
    numcells = 2 * n ** 2 + (n ** 2) ! 末項は四角形部分
    write(*,'(a, i0)') "節点数は ", numpoints
    write(*,'(a, i0)') "要素数は ", numcells

    allocate(rn(2 * n + 1),r(n + 1, 2 * n + 1), theta(2 * n + 1))
    allocate(x(numpoints), y(numpoints), a(numpoints, 2), p(numcells, numcellpoints))

    pi = acos(-1.0d0) ! 最初のtheta、単純に演習を分割しただけ
    !write(*,*) pi
    dtheta = (pi / 4) / n
    theta(n + 1) = pi / 4.0d0
    do i = 1, n
        if(i == n) then
            theta((n + 1) - n) = pi / 2.0d0
            theta((n + 1) + n) = 0.0d0
        else
            theta((n + 1) - i) = theta(n + 1) + i * dtheta
            theta((n + 1) + i) = theta(n + 1) - i * dtheta
        endif
    enddo

    s = 1.0d0 / n

    r0 = 0.1d0 ! 初項
    rnn = 1.0d0 * dsqrt(2.0d0) ! 1.0d0でよいか？
    do i = 1, 2 * n + 1 ! 末項
        if(i == n + 1) then
            rn(n + 1) =  rnn
        elseif(i < n + 1) then
            rn(i) = dsqrt(((i - 1) * s) ** 2 + (1.0d0) ** 2)
            theta(i) = asin(1.0d0 / rn(i))
        elseif(i > n + 1) then
            rn(i) = dsqrt((1.0d0) ** 2 + (((2 * n + 1 - i) * s) ** 2))
            theta(i) = acos(1.0d0 / rn(i))
        endif
    enddo

    do i = 1, n + 1 ! r
        do j = 1, 2 * n + 1
            kouhi = dexp(dlog(rn(j) / r0) / n)
            r(i, j) = r0 * kouhi ** (i - 1)
        enddo
    enddo

    do i = 1, 2 * n + 1 ! 確認
        write(*,'(a, i0, a, f10.5)') "theta(", i, ") = ", theta(i)
    enddo
    do i = 1, n + 1
        write(*,'(a, i0, a, f10.5)') "r(", i, ") = ", r(i, n + 1)
    enddo

    do i = 1, n + 1 ! r
        do j = 1, 2 * n + 1 ! theta
            if(i == 1) then
                if(j == 1) then
                    x(j) = 0.0d0
                    y(j) = r0
                elseif(j == 2 * n + 1) then
                    x(j) = r0
                    y(j) = 0.0d0
                else
                    x(j) = r0 * cos(theta(j))
                    y(j) = r0 * sin(theta(j))
                endif
            else
                if(j == 1) then
                    x((i - 1) * (2 * n + 1) + j) = 0.0d0
                    y((i - 1) * (2 * n + 1) + j) = r(i, j)
                elseif(j == 2 * n + 1) then
                    x((i - 1) * (2 * n + 1) + j) = r(i, j)
                    y((i - 1) * (2 * n + 1) + j) = 0.0d0
                else
                    x((i - 1) * (2 * n + 1) + j) = r(i, j) * cos(theta(j))
                    y((i - 1) * (2 * n + 1) + j) = r(i, j) * sin(theta(j))
                endif
            endif
        enddo
    enddo

    s = 1.0d0 / n ! 下の部分
    do i = 1, n
        do j = 1, n + 1
            if(i == 1) then
                x((2 * n ** 2 + 3 * n + 1) + j) = x((2 * n ** 2 + 3 * n + 1) + j - (2 * n + 1))
                y((2 * n ** 2 + 3 * n + 1) + j) = 1.0d0 + s
            elseif(i == n) then
                x((2 * n ** 2 + 3 * n + 1) + (i - 1) * (n + 1) + j) = x((2 * n ** 2 + 3 * n + 1) + j)
                y((2 * n ** 2 + 3 * n + 1) + (i - 1) * (n + 1) + j) = 2.0d0
            else
                x((2 * n ** 2 + 3 * n + 1) + (i - 1) * (n + 1) + j) = x((2 * n ** 2 + 3 * n + 1) + j)
                y((2 * n ** 2 + 3 * n + 1) + (i - 1) * (n + 1) + j) = 1.0d0 + i * s
            endif
        enddo
    enddo


    do i = 1, numpoints
        a(i, 1) = x(i)
        a(i, 2) = y(i)
    enddo

    do i = 1, numpoints ! 確認
        write(*, *) a(i, 1:2)
    enddo

    open(10, file = "node.dat" , status = "replace") ! node.dat 作成 ok
    write(10,'(i0)') numpoints
    do i = 1, numpoints
        write(10, *) a(i, 1:2)
    enddo
    close(10)

    do i = 1, n
        do j = 1, 2 * n
            p(j + (i - 1) * (2 * n), 1) = j + (i - 1) * (2 * n + 1)
            p(j + (i - 1) * (2 * n), 2) = j + (i - 1) * (2 * n + 1) + 1
            p(j + (i - 1) * (2 * n), 3) = j + i * (2 * n + 1) + 1
            p(j + (i - 1) * (2 * n), 4) = j + i * (2 * n + 1)
        enddo
    enddo

    do i = 1, n ! 下の部分
        do j = 1, n
            if(i == 1) then
                p(2 * n ** 2 + j, 1) = p((2 * n ** 2) + j - (2 * n), 4)
                p(2 * n ** 2 + j, 2) = p((2 * n ** 2) + j - (2 * n), 3)
                p(2 * n ** 2 + j, 3) = (2 * n ** 2) + (3 * n) + 1 + (j + 1)
                p(2 * n ** 2 + j, 4) = (2 * n ** 2) + (3 * n) + 1 + j
            else
                p(2 * n ** 2 + n * (i - 1) + j, 1) = (2 * n ** 2) + (3 * n) + 1 + (n + 1) * (i - 2) + j
                p(2 * n ** 2 + n * (i - 1) + j, 2) = (2 * n ** 2) + (3 * n) + 1 + (n + 1) * (i - 2) + (j + 1)
                p(2 * n ** 2 + n * (i - 1) + j, 3) = (2 * n ** 2) + (3 * n) + 1 + (n + 1) * (i - 1) + (j + 1)
                p(2 * n ** 2 + n * (i - 1) + j, 4) = (2 * n ** 2) + (3 * n) + 1 + (n + 1) * (i - 1) + j
            endif
        enddo
    enddo

    open(11, file = "elem.dat" , status = "replace")! elem.dat 作成 ok
    write(11, '(i0, a, i0)') numcells, " ", numcellpoints
    do i = 1, numcells
        do j = 1, numcellpoints
            if(j /= numcellpoints) then
                write(11, '(i0, a)', advance='no') p(i, j), " "
            else
                write(11, '(i0)') p(i, j)
            endif
        enddo
    enddo
    close(11)

    open(12, file = "bc.dat" , status = "replace")! bc.dat 作成ok
    write(12, '(i0, a, i0)') (2 * (n + 1) + n), " ", 2
    do i = 1, ((n + 1) + n)
        if(i <= (n + 1)) then
            write(12, '(i0, a, i0, a, f3.1)') (1 + (i - 1) * (2 * n + 1)), " ",  1, " ", 0.0d0
            write(12, '(i0, a, i0, a, f3.1)') (1 + (i - 1) * (2 * n + 1) + (2 * n)), " ", 2, " ", 0.0d0
        else
            write(12, '(i0, a, i0, a, f3.1)') ((2 * n ** 2) + (3 * n) + 2) + (i - (n + 2)) * (n + 1), " ",  1, " ", 0.0d0
        endif
    enddo
    close(12)

    open(13, file = "load.dat" , status = "replace")! load.dat、縦方向引張前提
    write(13, '(i0, a, i0)') (n + 1), " ", 2

    write(*,*) "長手方向の両端にかける荷重(N)："
    read(*,*) pow0

    pow = dble(pow0) / 2.0d0
    ppow = pow / (2 * n)

    do i = 1, n + 1
        if(i == 1 .or. i == n + 1) then
            write(13, '(i0, a, i0, a, f11.5)') (2 * n ** 2) + (3 * n) + 1 + ((n + 1) * (n - 1)) + i, " ",  2, " ", ppow
        else
            write(13, '(i0, a, i0, a, f11.5)') (2 * n ** 2) + (3 * n) + 1 + ((n + 1) * (n - 1)) + i, " ",  2, " ", 2 * ppow
        endif
    enddo
    close(13)

end program main