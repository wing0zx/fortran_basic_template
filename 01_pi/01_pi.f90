program main
  implicit none
  real(8) x, y, pi, pi0
  integer n, i, im
  write(*,*)"円周率を推定する乱数の生成数（試行回数）"
  read(*,*) im
  pi0 = 2.0d0 * acos(0.0d0)
  n = 0
  do i = 1, im
     call random_number(x)
     call random_number(y)
     if (x ** 2 + y ** 2 <= 1.0d0) n = n + 1
  enddo
  pi = 4.0d0 * dble(n) / dble(im)
  write(*,*)"推定円周率 ", pi
end program main